# Python for Finance: Meetup Notebook

This jupyter-notebook can be used to determine average returns of arbitrary stocks and funds. The notebook was used in the Meetup *Sparen am Aktienmarkt*.

For example, for an exchange traded funds based on the S&P500, the output will look like this for the years between 1983 and 2018: Return-Triangle S&P500:

![Triangle](renditedreieck_^GSPC.png)

# Installation

Please download and install anaconda from https://www.anaconda.com/products/individual. 

Change directory to /python-for-finance and
```console
$ conda create --name PythonForFinance --file requirements.txt
$ conda activate PythonForFinance
$ jupyter lab
```
